package com.ntst.ntstdroid.repository;

import com.ntst.ntstdroid.app.AppController;
import com.ntst.ntstdroid.database.AppDatabase;
import com.ntst.ntstdroid.model.User;
import com.ntst.ntstdroid.service.ApiService;
import com.ntst.ntstdroid.util.SharedPrefsUtil;

import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppRepository {

    private final AppController application;
    private final SharedPrefsUtil sharedPrefsUtil;
    private final ApiService apiService;
    private final AppDatabase database;
    private final Executor executor;

    @Inject
    public AppRepository(AppController application,
                         SharedPrefsUtil sharedPrefsUtil,
                         ApiService apiService,
                         AppDatabase db,
                         Executor executor) {

        this.application = application;
        this.sharedPrefsUtil = sharedPrefsUtil;
        this.apiService = apiService;
        this.database = db;
        this.executor = executor;
    }

    public User getUser(String email) {

        refreshUser(email);

        return database.userDao().findByEmail(email);
    }

    private void refreshUser(final String email) {

        executor.execute(() -> {

            boolean userExists = database.userDao().existUser(email);

            /*
            if (!userExists) {
                try {
                    // refresh the data
                    Response<User> response = apiService.getUser(email).execute();
                    // Update the database.The LiveData will automatically refresh so
                    // we don't need to do anything else here besides updating the database
                    database.userDao().save(response.body());
                } catch (IOException ioe) {

                }
            }
            */
        });
    }
}
