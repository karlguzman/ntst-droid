package com.ntst.ntstdroid.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.ntst.ntstdroid.R;
import com.ntst.ntstdroid.model.User;
import com.ntst.ntstdroid.repository.AppRepository;
import com.ntst.ntstdroid.util.Constants;
import com.ntst.ntstdroid.util.ResourceProvider;
import com.ntst.ntstdroid.util.SharedPrefsUtil;

import javax.inject.Inject;

public class LoginViewModel extends BaseViewModel {

    private AppRepository repository;
    private SharedPrefsUtil sharedPrefsUtil;
    private ResourceProvider resourceProvider;

    private MutableLiveData<User> user = new MutableLiveData<>();

    @Inject
    public LoginViewModel(AppRepository repository, SharedPrefsUtil sharedPrefsUtil, ResourceProvider resourceProvider) {
        this.repository = repository;
        this.sharedPrefsUtil = sharedPrefsUtil;
        this.resourceProvider = resourceProvider;
    }

    public boolean login() {

        if (validValues()) {

            User u = repository.getUser(email.get());

            if (u != null && u.getEmail().equals(email.get()) && u.getPassword().equals(password.get())) {

                user.setValue(u);

                sharedPrefsUtil.put(Constants.USER_ID_KEY, u.getId());
                sharedPrefsUtil.put(Constants.USERNAME_KEY, u.getEmail());

                // trigger the navigation event
                navigateToMainActivity.call();

                return true;

            } else {
                passwordError.set(resourceProvider.getString(R.string.error_incorrect_password));
            }
            sharedPrefsUtil.deleteSavedData(Constants.USER_ID_KEY);
            sharedPrefsUtil.deleteSavedData(Constants.USERNAME_KEY);
        }

        return false;
    }

    private boolean validValues() {

        emailError.set(null);
        passwordError.set(null);

        // Store values at the time of the login attempt.
        String em = this.email.get();
        String pass = this.password.get();

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(pass) || !isPasswordValid(pass)) {
            passwordError.set(resourceProvider.getString(R.string.error_invalid_password));
            return false;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(em)) {
            emailError.set(resourceProvider.getString(R.string.error_field_required));
            return false;

        } else if (!isEmailValid(em)) {
            emailError.set(resourceProvider.getString(R.string.error_invalid_email));
            return false;
        }
        return true;
    }

    private boolean isEmailValid(String email) {
        //TODO: Reemplazar con tus propias validaciones
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Reemplazar con tus propias validaciones
        return password.length() > 4;
    }

    public LiveData<User> getUser() {
        return user;
    }

    /**
     * Observe this event to navigate to the MainActivity
     */
    public final SingleLiveEvent<Void> navigateToMainActivity = new SingleLiveEvent<>();

    public final ObservableField<String> email = new ObservableField<>();
    public final ObservableField<String> password = new ObservableField<>();
    public final ObservableField<String> emailError = new ObservableField<>();
    public final ObservableField<String> passwordError = new ObservableField<>();
}
