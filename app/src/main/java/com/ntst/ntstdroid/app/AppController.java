package com.ntst.ntstdroid.app;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.ntst.ntstdroid.di.AppComponent;
import com.ntst.ntstdroid.di.DaggerAppComponent;

public class AppController extends Application {

    private static final String TAG = "AppController";

    private static AppController instance;

    private AppComponent appComponent;

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .application(this)
                .build();
        appComponent.inject(this);

        instance = this;

    }

    public static synchronized AppController getInstance() {
        return instance;
    }

    @SuppressWarnings("unused")
    public String getAppVersion() {

        String version = "";
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo( getPackageName(), 0 );
            version = info.versionName;
        } catch(Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return version;
    }
}
