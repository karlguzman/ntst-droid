package com.ntst.ntstdroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ntst.ntstdroid.util.Utils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final boolean isConnectedToNetwork = Utils.checkWIFIInternetConn() || Utils.checkMobileInternetConn();

        if (isConnectedToNetwork) {

            delay(1000L);
            workOffline();

        } else {
            delay(1001L);
            workOffline();
        }
    }

    private void delay(long millis) {

        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void workOffline() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
