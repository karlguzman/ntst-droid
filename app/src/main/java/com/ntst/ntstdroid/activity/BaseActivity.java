package com.ntst.ntstdroid.activity;

import android.support.annotation.LayoutRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.ntst.ntstdroid.R;
import com.ntst.ntstdroid.app.AppController;
import com.ntst.ntstdroid.model.User;
import com.ntst.ntstdroid.repository.AppRepository;
import com.ntst.ntstdroid.util.Constants;
import com.ntst.ntstdroid.util.SharedPrefsUtil;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected FloatingActionButton fab;

    protected boolean isModified =  false;

    @Inject
    AppRepository repository;
    @Inject
    SharedPrefsUtil sharedPrefsUtil;

    protected Drawer drawer;

    protected void setupDefaultControls() {

        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }

        // inject dependencies
        AppController.getInstance().getAppComponent().inject(this);

        String username = sharedPrefsUtil.get(Constants.USERNAME_KEY, "");
        User user = repository.getUser(username);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem().withName(user.getName()).withEmail(user.getEmail())
                        //.withIcon(getResources().getDrawable(R.drawable.profile))
                )
                .withOnAccountHeaderListener((view, profile, currentProfile) -> false ).build();


        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName("Home")
                .withIcon(GoogleMaterial.Icon.gmd_wb_sunny);

        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName("Settings1")
                .withIcon(FontAwesome.Icon.faw_github);

        //create the drawer and remember the `Drawer` result object
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withActionBarDrawerToggle(true)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        new SecondaryDrawerItem().withName("Settings").withIcon(FontAwesome.Icon.faw_address_card)
                )
                .build();

        fab = findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener((view) -> {

                if (!isModified) {
                    return;
                }
                save();
            });
        }
    }

    protected void save() {

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        setupDefaultControls();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }
}
