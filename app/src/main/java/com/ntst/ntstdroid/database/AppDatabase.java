package com.ntst.ntstdroid.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.ntst.ntstdroid.model.User;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase  extends RoomDatabase {
    public abstract UserDao userDao();
}
