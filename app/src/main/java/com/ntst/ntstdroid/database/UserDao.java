package com.ntst.ntstdroid.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.ntst.ntstdroid.model.User;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
@SuppressWarnings("unused")
public interface UserDao {

    @Query("SELECT * FROM user")
    LiveData<List<User>> getAll();

    @Query("SELECT * FROM user WHERE id = :userId")
    LiveData<User> loadById(int userId);

    @Query("SELECT * FROM user WHERE id IN (:userIds)")
    LiveData<List<User>> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user WHERE email LIKE :email1 LIMIT 1")
    User findByEmail(String email1);

    @Query("SELECT EXISTS (SELECT 1 FROM user WHERE email LIKE :email LIMIT 1)")
    boolean existUser(String email);

    @Insert(onConflict = REPLACE)
    long save(User user);

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);
}
