package com.ntst.ntstdroid.util;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Carlos Guzmán on 8/8/16.
 */
public class ColoredSnackbar {

    private static final int red = 0xfff44336;
    private static final int green = 0xff4caf50;
    private static final int blue = 0xff2195f3;
    private static final int orange = 0xffffc107;

    private static View getSnackBarLayout(Snackbar snackbar) {
        if (snackbar != null) {
            return snackbar.getView();
        }
        return null;
    }

    private static Snackbar colorSnackBar(Snackbar snackbar, int colorId) {

        return colorSnackBar(snackbar, colorId, false);
    }

    private static Snackbar colorSnackBar(Snackbar snackbar, int colorId, boolean showCloseAction) {
        View snackBarView = getSnackBarLayout(snackbar);

        if (snackBarView != null) {
            snackBarView.setBackgroundColor(colorId);

            TextView tv= (TextView)snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            tv.setMaxLines(10);
        }

        if (showCloseAction) {
            final Snackbar snb = snackbar;
            snb.setAction("Cerrar", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snb.dismiss();
                }
            });
        }
        return snackbar;
    }
    public static Snackbar info(Snackbar snackbar) {
        return colorSnackBar(snackbar, blue);
    }
    public static Snackbar info(Snackbar snackbar, boolean showCloseAction) {
        return colorSnackBar(snackbar, blue, showCloseAction);
    }

    public static Snackbar warning(Snackbar snackbar) {
        return colorSnackBar(snackbar, orange);
    }
    public static Snackbar warning(Snackbar snackbar, boolean showCloseAction) {
        return colorSnackBar(snackbar, orange, showCloseAction);
    }

    public static Snackbar alert(Snackbar snackbar) {
        return colorSnackBar(snackbar, red);
    }
    public static Snackbar alert(Snackbar snackbar, boolean showCloseAction) {
        return colorSnackBar(snackbar, red, showCloseAction);
    }

    public static Snackbar confirm(Snackbar snackbar) {
        return colorSnackBar(snackbar, green);
    }
    public static Snackbar confirm(Snackbar snackbar, boolean showCloseAction) {
        return colorSnackBar(snackbar, green, showCloseAction);
    }

    public static Snackbar make(@NonNull View view, @NonNull CharSequence text){

        @Snackbar.Duration
        int duration =  (text.length() * 100);

        return Snackbar.make(view, text, duration);

    }
}
