package com.ntst.ntstdroid.util;

import android.databinding.BindingAdapter;
import android.widget.EditText;

public class BindingUtil {

    @BindingAdapter("errorMsg")
    public static void setImageUrl(EditText editText, String errorMsg) {
        editText.setError(errorMsg);
    }
}
