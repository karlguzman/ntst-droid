package com.ntst.ntstdroid.util;

import android.content.Context;

import com.ntst.ntstdroid.app.AppController;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ResourceProvider
{
    private Context mContext;

    @Inject
    public ResourceProvider(AppController mContext) {
        this.mContext = mContext;
    }

    public String getString(int resId) {
        return mContext.getString(resId);
    }

    public String getString(int resId, String value) {
        return mContext.getString(resId, value);
    }
}
