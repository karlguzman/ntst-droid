package com.ntst.ntstdroid.util;

public class Constants {

    public static final String SHARED_PREFS_NAME = "NTST-DROID-PREFS";

    public static final String API_BASE_URL = "https://api.ntst.com";

    public static final String DATABASE_NAME = "NTST-DROID-DB";

    public static final String USER_ID_KEY ="USER_ID";

    public static final String USERNAME_KEY ="USERNAME";
}
