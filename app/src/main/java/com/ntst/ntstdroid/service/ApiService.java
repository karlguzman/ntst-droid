package com.ntst.ntstdroid.service;

import com.ntst.ntstdroid.model.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("users/{id}")
    Call<User> getUser(@Path("id") String email);
}
