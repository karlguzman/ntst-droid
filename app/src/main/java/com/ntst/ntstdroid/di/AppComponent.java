package com.ntst.ntstdroid.di;

import com.ntst.ntstdroid.activity.BaseActivity;
import com.ntst.ntstdroid.activity.LoginActivity;
import com.ntst.ntstdroid.activity.MainActivity;
import com.ntst.ntstdroid.app.AppController;
import com.ntst.ntstdroid.database.AppDatabase;
import com.ntst.ntstdroid.repository.AppRepository;
import com.ntst.ntstdroid.util.ResourceProvider;
import com.ntst.ntstdroid.util.SharedPrefsUtil;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    // inject Activities
    void inject(BaseActivity baseActivity);
    void inject(LoginActivity loginActivity);
    void inject(MainActivity mainActivity);

    void inject(AppController appController);

    SharedPrefsUtil getSharedPrefsUtil();
    AppRepository getDefaultRepository();
    AppDatabase getDatabase();
    ResourceProvider getResourceProvider();

    @Component.Builder
    interface Builder {

        AppComponent build();
        @BindsInstance Builder application(AppController application);
    }
}
