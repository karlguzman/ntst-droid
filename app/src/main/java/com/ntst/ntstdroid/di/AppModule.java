package com.ntst.ntstdroid.di;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

import com.ntst.ntstdroid.app.AppController;
import com.ntst.ntstdroid.database.AppDatabase;
import com.ntst.ntstdroid.model.User;
import com.ntst.ntstdroid.service.ApiService;
import com.ntst.ntstdroid.util.Constants;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {ViewModelModule.class})
public class AppModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs(AppController application) {
        return application.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    AppDatabase provideDatabase(AppController application, Executor executor) {

        //AppDatabase db = Room.databaseBuilder(application, AppDatabase.class, Constants.DATABASE_NAME).build();
        AppDatabase db = Room.inMemoryDatabaseBuilder(application, AppDatabase.class).build();

        //TODO: code below should be removed, is only for testing purposes.
        executor.execute(() -> db.userDao().save(User.getTestUser()));

        return db;
    }

    @Provides
    Executor getExecutor() {
        return Executors.newSingleThreadExecutor();
    }


}
