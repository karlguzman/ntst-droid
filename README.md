
# NTST-Droid

This is an android app template using Android Architecture Components (ViewModel, LiveData, Room), Dagger 2 for dependency injection and Retrofit for network requests.

*This is a work in progress...*

## Getting Started

Just download the code, compile and run the app on an android device or a simulator.

### Prerequisites

Android Studio IDE for Win or Mac.

## Built With

* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/) - Libraries used to implement MVVM
* [Dagger 2](https://google.github.io/dagger/) - Dependency Management
* [Retrofit](http://square.github.io/retrofit/) - Http client used


## Authors

* **Carlos Guzmán** - *Initial work* - [LinkedIn](www.linkedin.com/in/karlguzman)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
